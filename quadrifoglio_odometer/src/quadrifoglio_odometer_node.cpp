#include "ros/ros.h"
#include "std_msgs/Float32.h"
#include "geometry_msgs/Vector3.h"
//#include "sensor_msgs/PointCloud2.h"
#include "quadrifoglio_msgs/encoders.h"
#include <tf/LinearMath/Vector3.h>
#include <vector>

#define TODEG 57.29577951          //Conversion factors for degrees & radians
#define TORAD 0.017453293         //The AVR cannot reach this level of precision but w/e

const double wheelbase = 25.3;  //Robot wheelbase and track in cm, for ackerman angle calculations
const double track = 15.5;
const double maxSpeed = 1.0;  //Maximum robot speed in m/s


float Sign(float it){
    if(it<0) return -1.0;
    else return 1.0;
}

geometry_msgs::Vector3 toMsg(tf::Vector3 in){
    geometry_msgs::Vector3 out;
    out.x = in.x();
    out.y = in.y();
    out.z = in.z();
    return out;
}

class WheelModule{
    public:
    WheelModule(){
        _error = 0.0;
        _which = 0;
    }
    void Init(int which, double x, double y){
        _which = which;
        _pos.setX(x);
        _pos.setY(y);
        
    }
    void ResetError(){
        _error = 0.0;
    }
    void ErrorWRT(WheelModule other){
        if(_which != other._which){   //Only calculate error wrt other wheels, not self
            tf::Vector3 diff = other._vel - _vel;  //Difference of velocity vectors of the two wheels
            tf::Vector3 dir = other._pos - _pos;  //Direction of the error in wheel velocities, distance between
            //wheels should remain constant so any component which breaks this is a result of measurement
            //errors.
            _error += diff.dot(dir/dir.length());  //Projection of velocity onto the error direction unit vector
            //gives the component of the velocity vector which must be in error
        }
    }

    tf::Vector3 _pos;
    tf::Vector3 _vel;
    double _error;
    int _which;
};


class Odometer{
    public:
    Odometer(ros::Publisher& flPub, ros::Publisher& frPub, ros::Publisher& rlPub, ros::Publisher& rrPub):
    _flPub(flPub), _frPub(frPub), _rlPub(rlPub), _rrPub(rrPub){ 
        
        //Create vectors from robot center to wheel joints
        to_fl.setX(wheelbase/2);  //X is forward
        to_fl.setY(track/2);  //X is left
        to_fr.setX(wheelbase/2);  //X is forward
        to_fr.setY(-track/2);  //X is left
        to_rl.setX(-wheelbase/2);  //X is forward
        to_rl.setY(track/2);  //X is left
        to_rr.setX(-wheelbase/2);  //X is forward
        to_rr.setY(-track/2);  //X is left

        flm.Init(1,wheelbase/2,track/2);
        frm.Init(2,wheelbase/2,-track/2);
        rlm.Init(3,-wheelbase/2,track/2);
        rrm.Init(4,-wheelbase/2,-track/2);
        modules.push_back(&flm);
        modules.push_back(&frm);
        modules.push_back(&rlm);
        modules.push_back(&rrm);


    }
    void OdoCallback(const quadrifoglio_msgs::encoders::ConstPtr& enc){
        double elapsed = (ros::Time::now()-lastOdoTime).toSec();
        lastOdoTime = ros::Time::now();
        
        //Calculate velocity vectors for wheels
        flm._vel.setX(enc->frontLeftVel*cos(enc->frontLeftAngle*TORAD));
        flm._vel.setY(enc->frontLeftVel*sin(enc->frontLeftAngle*TORAD));
        frm._vel.setX(enc->frontRightVel*cos(enc->frontRightAngle*TORAD));
        frm._vel.setY(enc->frontRightVel*sin(enc->frontRightAngle*TORAD));
        rlm._vel.setX(enc->rearLeftVel*cos(enc->rearLeftAngle*TORAD));
        rlm._vel.setY(enc->rearLeftVel*sin(enc->rearLeftAngle*TORAD));
        rrm._vel.setX(enc->rearRightVel*cos(enc->rearRightAngle*TORAD));
        rrm._vel.setY(enc->rearRightVel*sin(enc->rearRightAngle*TORAD));

        //Calculate velocity vectors for wheels
        fl.setX(enc->frontLeftVel*cos(enc->frontLeftAngle*TORAD));
        fl.setY(enc->frontLeftVel*sin(enc->frontLeftAngle*TORAD));
        fr.setX(enc->frontRightVel*cos(enc->frontRightAngle*TORAD));
        fr.setY(enc->frontRightVel*sin(enc->frontRightAngle*TORAD));
        rl.setX(enc->rearLeftVel*cos(enc->rearLeftAngle*TORAD));
        rl.setY(enc->rearLeftVel*sin(enc->rearLeftAngle*TORAD));
        rr.setX(enc->rearRightVel*cos(enc->rearRightAngle*TORAD));
        rr.setY(enc->rearRightVel*sin(enc->rearRightAngle*TORAD));

        //For left front wheel
        tf::Vector3 diff = fl - fr;
        tf::Vector3 dir = (to_fl - to_fr);
        float err1 = diff.dot(dir/dir.length());

        //ROS_INFO("diff x %f diff y %f diff z %f dir x %f dir y %f dir z %f err %f",
        //diff.x(),diff.y(),diff.z(),dir.x(),dir.y(),dir.z(),err1);

        for(WheelModule* module : modules){
            module->ResetError();
            for(WheelModule* other : modules){ //Add error wrt each other module
                module->ErrorWRT(*other);  //It skips comparison to itself
            }
        }

        ROS_INFO("err fl: %f err fr: %f err rl: %f err rr: %f",
        flm._error, frm._error, rlm._error, rrm._error);


        geometry_msgs::Vector3 toSend;
        toSend.x = fl.x(); toSend.y = fl.y(); toSend.z = fl.z(); _flPub.publish(toSend);
        toSend.x = fr.x(); toSend.y = fr.y(); toSend.z = fr.z(); _frPub.publish(toSend);
        toSend.x = rl.x(); toSend.y = rl.y(); toSend.z = rl.z(); _rlPub.publish(toSend);
        toSend.x = rr.x(); toSend.y = rr.y(); toSend.z = rr.z(); _rrPub.publish(toSend);
        
    }

    private:
    ros::Publisher &_flPub;
    ros::Publisher &_frPub;
    ros::Publisher &_rlPub;
    ros::Publisher &_rrPub;

    tf::Vector3 fl;
    tf::Vector3 fr;
    tf::Vector3 rl;
    tf::Vector3 rr;
    tf::Vector3 to_fl;
    tf::Vector3 to_fr;
    tf::Vector3 to_rl;
    tf::Vector3 to_rr;

    WheelModule flm;
    WheelModule frm;
    WheelModule rlm;
    WheelModule rrm;
    std::vector<WheelModule*> modules;

    ros::Time lastOdoTime;  //Timestamp for computing velocities from position
    double _ticToMeter;
};


int main(int argc, char **argv){

    ros::init(argc, argv, "quadrifoglio_odometer");
    ros::NodeHandle n;


    ros::Publisher flPub = n.advertise<geometry_msgs::Vector3>("vec_fl",1);
    ros::Publisher frPub = n.advertise<geometry_msgs::Vector3>("vec_fr",1);
    ros::Publisher rlPub = n.advertise<geometry_msgs::Vector3>("vec_rl",1);
    ros::Publisher rrPub = n.advertise<geometry_msgs::Vector3>("vec_rr",1);



    Odometer odometer(flPub, frPub, rlPub, rrPub);

    ros::Subscriber encoderSub = n.subscribe("quadrifoglio/encoders", 1, &Odometer::OdoCallback, &odometer);
    ROS_INFO("Starting odometer");

    ros::spin();

    return 0;
}
